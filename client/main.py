import requests 
from configuration import url

def gets_mots():
    """ Récupère les mots de l'API"""
    requete = requests.get("http://localhost:8000/mots")
    return requete.json()

def add_mot(identifiant, carac):
    """ Ajoute le mot sur l'API """
    myMot= {"id":identifiant,"caracteres":carac}
    requete = requests.post("http://localhost:8000/mot", json = myMot)
    return requete.json()

print(add_mot(identifiant=3, carac = "Poulet"))
print(gets_mots())
